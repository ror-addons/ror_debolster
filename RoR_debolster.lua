RoR_debolster = {}
RoR_debolster.Tier = {}
RoR_debolster.Tier[0] = {Rank=41,CheckTalis=false,Mastery=80 , level=80, Renown=80}
RoR_debolster.Tier[1] = {Rank=15,CheckTalis=true,Mastery=2 , level=13, Renown=13}
RoR_debolster.Tier[2] = {Rank=30,CheckTalis=false,Mastery=13 , level=28, Renown=28}
RoR_debolster.Tier[3] = {Rank=30,CheckTalis=false,Mastery=13 , level=28, Renown=28}
RoR_debolster.Tier[4] = {Rank=41,CheckTalis=false,Mastery=80 , level=80, Renown=80}
RoR_debolster.Tier[5] = {Rank=51,CheckTalis=false,Mastery=90 , level=90, Renown=90}
local version = "1.35"

function RoR_debolster.Initialize()
RoR_debolster.ForceShow = false
LibSlash.RegisterSlashCmd("debolster", function() RoR_debolster.ToggleWindow() end)

TextLogAddEntry("Chat", 0, L"<icon57> Debolster "..towstring(version)..L" Loaded")
	
		CreateWindow("RoR_debolsterWindow", false)
			CreateWindow("RoR_debolsterMasteryWindow", false)
		CreateWindow("RoR_debolsterRenownWindow", false)
		CreateWindow("RoR_debolsterTalismanWindow", false)
				CreateWindow("RoR_debolsterOutRankedWindow", false)
						CreateWindow("RoR_debolsterTrophyWindow", false)		
				
	
			RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "RoR_debolster.check")
			RegisterEventHandler(SystemData.Events.PLAYER_EQUIPMENT_SLOT_UPDATED, "RoR_debolster.check")
			RegisterEventHandler(SystemData.Events.UPDATE_ITEM_ENHANCEMENT, "RoR_debolster.check")
			RegisterEventHandler(SystemData.Events.END_ITEM_ENHANCEMENT, "RoR_debolster.check")
			RegisterEventHandler(SystemData.Events.PLAYER_RVR_FLAG_UPDATED, "RoR_debolster.check")
			RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "RoR_debolster.check")
			RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RANK_UPDATED, "RoR_debolster.check")
			RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "RoR_debolster.check")
			RegisterEventHandler(SystemData.Events.PLAYER_TROPHY_SLOT_UPDATED, "RoR_debolster.check")
		

 --   WindowRegisterEventHandler("EA_Window_InteractionRenownTraining", SystemData.Events.PLAYER_CAREER_CATEGORY_UPDATED,  "RoR_debolster.check")
 --   WindowRegisterEventHandler("EA_Window_InteractionRenownTraining", SystemData.Events.PLAYER_ABILITIES_LIST_UPDATED,   "RoR_debolster.check" )
--    WindowRegisterEventHandler("EA_Window_InteractionRenownTraining", SystemData.Events.PLAYER_SINGLE_ABILITY_UPDATED,   "RoR_debolster.check" )
 --   WindowRegisterEventHandler("EA_Window_InteractionRenownTraining", SystemData.Events.PLAYER_MONEY_UPDATED,            "RoR_debolster.check" )    

	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_CATEGORY_UPDATED, "RoR_debolster.check")
		
				
	RoR_debolster.check()

end

function RoR_debolster.ToggleWindow()
if RoR_debolster.ForceShow == false then  
RoR_debolster.ForceShow = true
RoR_debolster.check()
else
RoR_debolster.ForceShow = false
end

end


function RoR_debolster.CloseWindow()
RoR_debolster.ForceShow = false

		WindowSetShowing("RoR_debolsterWindow", false)
		WindowSetShowing("RoR_debolsterMasteryWindow", false)
		WindowSetShowing("RoR_debolsterRenownWindow", false)
		WindowSetShowing("RoR_debolsterTalismanWindow", false)
		WindowSetShowing("RoR_debolsterOutRankedWindow", false)
		WindowSetShowing("RoR_debolsterTrophyWindow", false)


end


function RoR_debolster.check()
		WindowSetShowing("RoR_debolsterWindow", false)
		WindowSetShowing("RoR_debolsterMasteryWindow", false)
		WindowSetShowing("RoR_debolsterRenownWindow", false)
		WindowSetShowing("RoR_debolsterTalismanWindow", false)
		WindowSetShowing("RoR_debolsterOutRankedWindow", false)
		WindowSetShowing("RoR_debolsterTrophyWindow", false)
local TierLocation = 4
	
if RoR_debolster.ForceShow == true then
	WindowSetShowing("RoR_debolsterWindow", true)	
end
	
if not GameData.Player.isInScenario then
if GameData.Player.zone == nil then
--if (not GetCampaignZoneData(GameData.Player.zone).tierId) or GetCampaignZoneData(GameData.Player.zone).tierId == nil then 
TierLocation = 4 
else
if (not GetCampaignZoneData(GameData.Player.zone).tierId)  or (GetCampaignZoneData(GameData.Player.zone).tierId == nil) then 
TierLocation = 4 
else
TierLocation = GetCampaignZoneData(GameData.Player.zone).tierId --GameData.Player.zone 106
end
end

else
TierLocation = 5
end


if GameData.Player.level > RoR_debolster.Tier[TierLocation].Rank then
	LabelSetText("RoR_debolsterWindowTitle",L"Tier "..towstring(TierLocation)..L" Debolster")
	LabelSetText("RoR_debolsterWindowText",L"You are too Powerfull for RvR in this Tier.<br>To continue Fighting in this Tier you need to adress the following Issues:")

if GameData.Player.rvrPermaFlagged or GameData.Player.rvrZoneFlagged or RoR_debolster.ForceShow == true then


for i=0,30 do
if( DoesWindowExist( "DeBolsterTestTalis"..i ) ) then
DestroyWindow("DeBolsterTestTalis"..i)
end
if( DoesWindowExist( "DeBolsterTestLevel"..i ) ) then
DestroyWindow("DeBolsterTestLevel"..i)
end
if( DoesWindowExist( "DeBolsterTestTrophy"..i ) ) then
DestroyWindow("DeBolsterTestTrophy"..i)
end

end

  EA_Window_InteractionSpecialtyTraining.LoadAdvances()
  EA_Window_InteractionSpecialtyTraining.Refresh()
  EA_Window_InteractionRenownTraining.LoadAdvances()
  EA_Window_InteractionRenownTraining.Refresh()
  
	local BadEQCounterTalis = 0
	local BadEQCounterLevel = 0
	local BadEQCounterTroph = 0
	local MasteryPointsSpent = 0
	local RenownPointsSpent = EA_Window_InteractionRenownTraining.GetPointsSpent()

	
	EquipmentInfo = {}
	BAD_EquipmentInfo = {}
	BAD_EquipmentTalis = {}
--	local has_empty_slots = (g.configDlgSection.isActive)
	local equipment_data = DataUtils.GetEquipmentData()
	local trophy_data = DataUtils.GetTrophyData()
	
	local RenownSpent = EA_Window_InteractionRenownTraining.GetPointsSpent()
	local RenownAvalible = EA_Window_InteractionRenownTraining.GetPointsAvailable()
	local NumEQ = #GameData.EquipSlots
	
	--(DataUtils.GetTrophyData())
	
		for k,v in pairs (GameData.EquipSlots)
		do
			local item = equipment_data[v]			
			EquipmentInfo[v] = {}
			EquipmentInfo[v].HasTalismanSlotted = false

	if not (item.level == nil) and (item.level > RoR_debolster.Tier[TierLocation].level ) then --9
	
	EquipmentInfo[v].ItemLevel = item.level
	EquipmentInfo[v].Level = item.level
	EquipmentInfo[v].Name = item.name
	EquipmentInfo[v].EQSlot = item.equipSlot
	EquipmentInfo[v].HasTalismanSlotted = false
	
	BAD_EquipmentInfo[BadEQCounterLevel] = {}
	BAD_EquipmentInfo[BadEQCounterLevel].Name = item.name
	BAD_EquipmentInfo[BadEQCounterLevel].info = L"GearLevel Too high"
	BAD_EquipmentInfo[BadEQCounterLevel].level = item.level
	BAD_EquipmentInfo[BadEQCounterLevel].EQslot = item.equipSlot
	BAD_EquipmentInfo[BadEQCounterLevel].rarity = item.rarity
	BAD_EquipmentInfo[BadEQCounterLevel].icon = item.iconNum
	BAD_EquipmentInfo[BadEQCounterLevel].SlotNumber = v





	
	LabelSetText("RoR_debolsterOutRankedWindowTitle",L"意o Equipment Over Minimum Rank "..towstring(RoR_debolster.Tier[TierLocation].level))		
	local texture =( item.iconNum ) 
	
	
							if( not DoesWindowExist( "DeBolsterTestLevel"..BadEQCounterLevel ) ) then
							CreateWindowFromTemplate( "DeBolsterTestLevel"..BadEQCounterLevel, "RoR_debolsterLabelTemplate", "RoR_debolsterOutRankedWindow" )
							WindowSetParent("DeBolsterTestLevel"..BadEQCounterLevel,"RoR_debolsterOutRankedWindow")
						LabelSetText("DeBolsterTestLevel"..BadEQCounterLevel.."Text",L"<icon"..towstring(texture)..L">"..towstring(EA_Window_Inspection.EquipmentSlotInfo[v].name)..L" - "..towstring(item.name)..L" ("..towstring(item.level)..L")")
						local colour = DataUtils.GetItemRarityColor(item)
					--	WindowSetTintColor("DeBolsterTestLevel"..BadEQCounterLevel.."Text", colour.r, colour.g, colour.b)			
						LabelSetTextColor("DeBolsterTestLevel"..BadEQCounterLevel.."Text", colour.r, colour.g, colour.b)			

					end

	
	BadEQCounterLevel = BadEQCounterLevel+1

	else
		if (item.numEnhancementSlots > 0 and #item.enhSlot ~= 0) and RoR_debolster.Tier[TierLocation].CheckTalis == true then
	
			EquipmentInfo[v].HasTalismanSlotted = true
			has_empty_slots = false
			EquipmentInfo[v].ItemLevel = item.level
			EquipmentInfo[v].Name = item.name
			EquipmentInfo[v].EQSlot = item.equipSlot
			
				BAD_EquipmentTalis[BadEQCounterTalis] = {}
				BAD_EquipmentTalis[BadEQCounterTalis].Name = item.name
				BAD_EquipmentTalis[BadEQCounterTalis].info = L"Talisman Equipped"
				BAD_EquipmentTalis[BadEQCounterTalis].EQslot = item.equipSlot
				BAD_EquipmentTalis[BadEQCounterTalis].rarity = item.rarity
				BAD_EquipmentTalis[BadEQCounterTalis].ID = item.id	
				
--						local colour = DataUtils.GetItemRarityColor(itemData)
--		WindowSetTintColor(self.name, colour.r, colour.g, colour.b)
				
				LabelSetText("RoR_debolsterTalismanWindowTitle",L"意o Equipment With Talismans")
				local texture =( item.iconNum ) 
							if( not DoesWindowExist( "DeBolsterTestTalis"..BadEQCounterTalis ) ) then
							CreateWindowFromTemplate( "DeBolsterTestTalis"..BadEQCounterTalis, "RoR_debolsterLabelTemplate", "RoR_debolsterTalismanWindow" )
							WindowSetParent("DeBolsterTestTalis"..BadEQCounterTalis,"RoR_debolsterTalismanWindow")
							LabelSetText("DeBolsterTestTalis"..BadEQCounterTalis.."Text",L"<icon"..towstring(texture)..L">"..towstring(EA_Window_Inspection.EquipmentSlotInfo[item.equipSlot].name)..L" - "..towstring(item.name))
							end

							BadEQCounterTalis = BadEQCounterTalis+1	
				end	

			

if BadEQCounterTalis == 0 or RoR_debolster.Tier[TierLocation].CheckTalis == false then
WindowSetDimensions("RoR_debolsterTalismanWindow",400,0)
WindowSetShowing("RoR_debolsterTalismanWindow",false)
elseif BadEQCounterTalis > 0 and RoR_debolster.Tier[TierLocation].CheckTalis == true then
WindowSetDimensions("RoR_debolsterTalismanWindow",400,28+(BadEQCounterTalis*22))

WindowSetShowing("RoR_debolsterWindow", true)
WindowSetShowing("RoR_debolsterTalismanWindow", true)
end

end		
	
	
	for i=0,BadEQCounterTalis do
		if(DoesWindowExist("DeBolsterTestTalis"..i) ) then
	WindowSetOffsetFromParent("DeBolsterTestTalis"..i,0,28+(tonumber(i)*22))
		end
	end
	for i=0,BadEQCounterLevel do
		if(DoesWindowExist("DeBolsterTestLevel"..i) ) then
	WindowSetOffsetFromParent("DeBolsterTestLevel"..i,0,28+(tonumber(i)*22))	
		end
	end
	
	end
		
			

--	d(BAD_EquipmentInfo)
--	d(L"RR points spent: "..towstring(RenownSpent))

--Mastery calculations
	for k,v in pairs (EA_Window_InteractionSpecialtyTraining.initialSpecializationLevels) do
	MasteryPointsSpent = MasteryPointsSpent+v
	end
	
--	d(L"Mastery spent: "..towstring(MasteryPointsSpent))
--	d(L"talis: "..towstring(BadEQCounterTalis))
--	d(L"Level: "..towstring(BadEQCounterLevel))

--Trophy

BAD_TrophyInfo = {}
for i = 1,5 do

local Trophy = trophy_data[i]		
	if not (Trophy.level == nil) and (Trophy.level > RoR_debolster.Tier[TierLocation].level ) then --9

	BAD_TrophyInfo[i] = {}
	BAD_TrophyInfo[i].Name = Trophy.name
	BAD_TrophyInfo[i].info = L"GearLevel Too high"
	BAD_TrophyInfo[i].level = Trophy.level
	BAD_TrophyInfo[i].EQslot = Trophy.equipSlot
	BAD_TrophyInfo[i].rarity = Trophy.rarity
	BAD_TrophyInfo[i].icon = Trophy.iconNum
	BAD_TrophyInfo[i].SlotNumber = 20+i

		LabelSetText("RoR_debolsterTrophyWindowTitle",L"意o Throphys Over Level "..towstring(RoR_debolster.Tier[TierLocation].level))		
		local texture = Trophy.iconNum  
	
	
							if( not DoesWindowExist( "DeBolsterTestTrophy"..BadEQCounterTroph ) ) then
							CreateWindowFromTemplate( "DeBolsterTestTrophy"..BadEQCounterTroph, "RoR_debolsterLabelTemplate", "RoR_debolsterTrophyWindow" )
							WindowSetParent("DeBolsterTestTrophy"..BadEQCounterTroph,"RoR_debolsterTrophyWindow")
						LabelSetText("DeBolsterTestTrophy"..BadEQCounterTroph.."Text",L"<icon"..towstring(texture)..L">"..L"Trophy"..towstring(i)..L" - "..towstring(Trophy.name)..L" ("..towstring(Trophy.level)..L")")
						local colour = DataUtils.GetItemRarityColor(Trophy)
						LabelSetTextColor("DeBolsterTestTrophy"..BadEQCounterTroph.."Text", colour.r, colour.g, colour.b)			
						end
	BadEQCounterTroph = BadEQCounterTroph+1	
	
	
end

end
	for i=0,BadEQCounterTroph do
		if(DoesWindowExist("DeBolsterTestTrophy"..i) ) then
	WindowSetOffsetFromParent("DeBolsterTestTrophy"..i,0,28+(tonumber(i)*22))
		end
	end



	
--Renown
if GameData.Player.Renown.curRank >= GameData.Player.level and (GameData.Player.level < 40)then
MaxRenown = GameData.Player.level
else
MaxRenown = GameData.Player.Renown.curRank
end

--
local CalcRenown = RenownSpent - RenownAvalible 
--local CalcRenown = MaxRenown - RenownAvalible 





if CalcRenown > RoR_debolster.Tier[TierLocation].Renown then --9
		WindowSetShowing("RoR_debolsterRenownWindow", true)
		
	LabelSetText("RoR_debolsterRenownWindowRenownTitle",L"意o More than "..towstring(RoR_debolster.Tier[TierLocation].Renown)..L" Renown points Spent")
	LabelSetText("RoR_debolsterRenownWindowRenownText",L"You Have Spent "..towstring(CalcRenown)..L"<icon45>Renown Point(s)")
	WindowSetDimensions("RoR_debolsterRenownWindow",400,50)
	WindowSetShowing("RoR_debolsterWindow", true)
	WindowSetShowing("RoR_debolsterRenownWindow",true)
else
	WindowSetDimensions("RoR_debolsterRenownWindow",400,0)	
end
	
	
	--Mastery
if MasteryPointsSpent > RoR_debolster.Tier[TierLocation].Mastery then
			WindowSetShowing("RoR_debolsterMasteryWindow", true)
			WindowSetShowing("RoR_debolsterWindow", true)

		
--if TierLocation == 1 then
--	LabelSetText("RoR_debolsterMasteryWindowMasteryTitle",L"意o Mastery points Spent")
--	else
	LabelSetText("RoR_debolsterMasteryWindowMasteryTitle",L"意o More than "..towstring(RoR_debolster.Tier[TierLocation].Mastery)..L" Master points Spent")
--	end
	
	LabelSetText("RoR_debolsterMasteryWindowMasteryText",L"You Have Spent "..towstring(MasteryPointsSpent)..L"<icon41>Mastery Point(s)")
			WindowSetDimensions("RoR_debolsterMasteryWindow",400,50)		
	else
		WindowSetDimensions("RoR_debolsterMasteryWindow",400,0)	
	
end
	
	
if BadEQCounterLevel == 0 then
WindowSetDimensions("RoR_debolsterOutRankedWindow",400,0)
WindowSetShowing("RoR_debolsterOutRankedWindow",false)
elseif BadEQCounterLevel > 0 then

WindowSetDimensions("RoR_debolsterOutRankedWindow",400,28+(BadEQCounterLevel*22))
WindowSetShowing("RoR_debolsterWindow", true)
WindowSetShowing("RoR_debolsterOutRankedWindow", true)		
end


if BadEQCounterTroph == 0 then
WindowSetDimensions("RoR_debolsterTrophyWindow",400,0)
WindowSetShowing("RoR_debolsterTrophyWindow", false)
elseif BadEQCounterTroph > 0 then

WindowSetDimensions("RoR_debolsterTrophyWindow",400,28+(BadEQCounterTroph*22))
WindowSetShowing("RoR_debolsterWindow", true)
WindowSetShowing("RoR_debolsterTrophyWindow", true)		
end

	
			
	
	
local MasteryWindowWidth,MasteryWindowHeight = WindowGetDimensions("RoR_debolsterMasteryWindow")
local RenownWindowWidth,RenownWindowHeight = WindowGetDimensions("RoR_debolsterRenownWindow")
local TalismanWindowWidth,TalismanWindowHeight = WindowGetDimensions("RoR_debolsterTalismanWindow")
local OutRankedWidth,OutRankedWindowHeight = WindowGetDimensions("RoR_debolsterOutRankedWindow")
local TrophysWidth,TrophysHeight = WindowGetDimensions("RoR_debolsterTrophyWindow")

WindowSetDimensions("RoR_debolsterWindow",400,MasteryWindowHeight+RenownWindowHeight+TalismanWindowHeight+OutRankedWindowHeight+TrophysHeight+90)


else
		WindowSetShowing("RoR_debolsterWindow", false)
		WindowSetShowing("RoR_debolsterMasteryWindow", false)
		WindowSetShowing("RoR_debolsterRenownWindow", false)
		WindowSetShowing("RoR_debolsterTalismanWindow", false)
		WindowSetShowing("RoR_debolsterOutRankedWindow", false)
		WindowSetShowing("RoR_debolsterTrophyWindow", false)
end
else
	LabelSetText("RoR_debolsterWindowTitle",L"Tier "..towstring(TierLocation)..L" Debolster")
	LabelSetText("RoR_debolsterWindowText",L"You are able too do RvR in this Tier.<br>Your Equipment and Specc seems to be within the tier rules")
WindowSetDimensions("RoR_debolsterWindow",400,85)


end
end