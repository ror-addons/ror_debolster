<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="RoR_debolster" version="1.35" date="27/02/2017" >
	<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" /> 
		<Author name="Sullemunk" />
		<Description text="Debolster Check" />
		     <Dependencies>
            <Dependency name="EA_InspectionWindow" />
			<Dependency name="LibSlash" />  
		</Dependencies>
		<Files>
			<File name="RoR_debolster.lua" />
			<File name="RoR_debolster.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="RoR_debolster.Initialize" /> 
		</OnInitialize>
	</UiMod>
</ModuleFile>
